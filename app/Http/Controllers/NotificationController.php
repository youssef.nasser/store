<?php

namespace App\Http\Controllers;

use App\Models\User;


class NotificationController extends Controller
{
    public function index ($user_id)
    {

        $user = User::find($user_id);
        $notifications = $user->notifications()->get();

        return view('notifications.index',[
            'user' => $user,
            'notifications' => $notifications
        ]);
    }

    public function read($notification_id)
    {
        $notify = auth()->user()->notifications()->find($notification_id);
        $notify->read_at = today();
        $notify->save();
        return redirect()->back();
    }

    public function destroy($notification_id)
    {
        $notify = auth()->user()->notifications()->find($notification_id);
        $notify->delete();
        return redirect()->back();
    }

}


