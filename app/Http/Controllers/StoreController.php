<?php

namespace App\Http\Controllers;


use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StoreController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $wallet = $user->wallet;
        $stores = $user->is_owner ? $user->stores()->get() : Store::get();
        $notify_count = $user->notifications()->whereNull('read_at')->count();

        return view('stores.index', [
            'user_id'       => $user->id,
            'stores'        => $stores,
            'wallet'        => $wallet,
            'notify_count'  => $notify_count,
        ]);
    }

    public function create()
    {
        $this->authorize('create', [App\Models\Store::class]);

        $user = auth()->user();
        $notify_count = $user->notifications()->count();

        return view('stores.create', [
            'notify_count' => $notify_count,
        ]);
    }

    public function store(Request $request)
    {
        $this->authorize('create', [App\Models\Store::class]);

        $data = $request->validate([
            'name'  => 'required|string',
            'about' => 'required',
            'phone' => 'required',
            'image' => 'sometimes|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        if ($request->hasFile('image')) {
            $file          = $request->file('image');
            $extention     = $file->getClientOriginalExtension();
            $fileName      = time() . '.' . $extention;
            $file->move('uploads/stores', $fileName);
            $data['image'] = $fileName;
        }

        $wallet = Auth::user()->wallet()->first();

        if ($wallet->balance < 10 ) {
            return redirect()->back()->with('error', 'You need at least 10$');
        }

        $wallet->balance = $wallet->balance - 10;

        $wallet->save();

        Auth::user()->stores()->create($data);
        return to_route('stores.index');
    }

    public function edit($store_id)
    {
        $this->authorize('update', [App\Models\Store::class,  Store::find($store_id)]);
        $user = auth()->user();
        $notify_count = $user->notifications()->whereNull('read_at')->count();

        return view('stores.edit',[
            'store' =>  Store::find($store_id),
            'notify_count'  => $notify_count,
        ]);
    }

    public function update(Request $request, $store_id)
    {
        $this->authorize('update', [App\Models\Store::class,  Store::find($store_id)]);

        $store = Store::find($store_id);

        $data = $request->validate([
            'name'  => 'required|string',
            'about' => 'required',
            'phone' => 'required',
            'image' => 'sometimes|image|mimes:jpeg,png,jpg',
        ]);

        if($request->hasFile('image')){
            $file = $request->file('image');
            $extention = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $extention;
            $file->move('uploads/stores', $fileName);
            $data['image'] = $fileName;
        }

        Store::find($store_id)->update($data);

        return redirect()->route('stores.index')->with('message', 'Store updated successfully');
    }

    public function destroy ($store_id)
    {
        $this->authorize('delete', [App\Models\Store::class,  Store::find($store_id)]);
        Store::find($store_id)->delete();
        return to_route('stores.index');
    }
}
