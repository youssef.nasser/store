<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\Voucher;
use Illuminate\Http\Request;

class VoucherController extends Controller
{
    public function index($store_id)
    {
        $this->authorize('create',[App\Models\Product::class]);

        $store = Store::find($store_id);

        $user = auth()->user();

        $notify_count = $user->notifications()->whereNull('read_at')->count();

        $vouchers = $store->vouchers()->get();

        return view('vouchers.index',[
            'store' => $store,
            'vouchers' => $vouchers,
            'notify_count'  => $notify_count,
        ]);
    }

    public function create($store_id)
    {
        $store = Store::find($store_id);

        $user = auth()->user();

        $notify_count = $user->notifications()->whereNull('read_at')->count();

        return view('vouchers.create',[
            'store' => $store,
            'notify_count'  => $notify_count,
        ]);
    }

    public function store(Request $request, $store_id)
    {
        $data = $request->validate([
            'percentage' => 'required',
            'times' => 'required',
            'code' => 'required|string'
        ]);

        Store::find($store_id)->vouchers()->create($data);
        return to_route('vouchers.index', $store_id);
    }

    public function edit($voucher_id)
    {
        $voucher = Voucher::find($voucher_id);
        $store = $voucher->store->first();
        $user = auth()->user();
        $notify_count = $user->notifications()->whereNull('read_at')->count();

        return view('vouchers.edit',[
            'voucher' => $voucher,
            'user_id' => $user->id,
            'notify_count'  => $notify_count,
            'store' => $store,
        ]);
    }

    public function update(Request $request, $voucher_id)
    {

        $data = $request->validate([
            'percentage' => 'required',
            'times' => 'required',
            'code' => 'required|string'
        ]);

        $voucher= Voucher::find($voucher_id);
        $voucher->update($data);

        return to_route('vouchers.index', $voucher->store_id);
    }

    public function destroy($voucher_id)
    {
        Voucher::find($voucher_id)->delete();

        return redirect()->back();
    }
}
