<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\UserRegistered;

class AuthController extends Controller
{

    public function register()
    {
        return view('auth.register');
    }

    public function signup(Request $request)
    {

        $data = $request->validate([
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);

        $data['password'] = bcrypt($data['password']);

        if ($request->is_owner == "on"){
            $data['is_owner'] = true;
        } else {
            $data['is_owner'] = false;
        }

        $user = User::create($data);
        $user->notify(new UserRegistered($user));

        $user->wallet()->create([
            'balance' => $request->balance,
        ]);

        return to_route('login');
    }

    public function login()
    {
        return view('auth.login');
    }

    public function signin(Request $request)
    {
        $data = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($data)) {
            request()->session()->regenerate();

            return to_route('stores.index');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return to_route('login');
    }



}
