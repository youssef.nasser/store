<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Store;
use App\Models\Product;
use App\Models\Voucher;
use Illuminate\Http\Request;
use App\Notifications\ProductSold;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index($store_id)
    {
        $products= Store::find($store_id)->products()->get();
        $user = auth()->user();
        $notify_count = $user->notifications()->whereNull('read_at')->count();
        $wallet = $user->wallet;
        return view('products.index', [
            'store_id'      => $store_id,
            'products'      => $products,
            'user'       => $user,
            'notify_count'  => $notify_count,
            'wallet'        => $wallet,
        ]);
    }

    public function create($store_id)
    {
        $this->authorize('create',[App\Models\Product::class]);
        $user = auth()->user();
        $notify_count = $user->notifications()->whereNull('read_at')->count();
        return view('products.create', [
            'store_id' => $store_id,
            'user_id'  => $user->id,
            'notify_count'=>$notify_count,
        ]);
    }

    public function store(Request $request, $store_id)
    {
        $this->authorize('create', [Product::class]);
        $data = $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'quantity' => 'required',
            'price' => 'required',
        ]);
        $store = Store::find($store_id);

        $product = $store->products()->create($data);

        $images = $this->uploadImages($request->images);

        $product->images()->createMany($images);

        return to_route('products.index', $store_id);
    }

    public function show($product_id)
    {
        $product = Product::find($product_id);
        $images = $product->Images;


        $user = auth()->user();
        $notify_count = $user->notifications()->whereNull('read_at')->count();
        $wallet = $user->wallet;

        return view('products.show',[
            'product' => $product,
            'images' => $images,
            'user' => $user,
            'notify_count' => $notify_count,
            'wallet'   => $wallet,
        ]);
    }

    public function destroy($product_id)
    {
        $this->authorize('delete', Product::find($product_id));
        Product::find($product_id)->delete();
        return redirect()->back();
    }

    public function edit($product_id)
    {
        $product = Product::find($product_id);

        $this->authorize('update',$product);

        $user = auth()->user();
        $notify_count = $user->notifications()->whereNull('read_at')->count();

        return view('products.edit', [
            'product' => $product,
            'user_id' => $user->id,
            'notify_count' => $notify_count,
        ]);
    }
    public function update(Request $request, $product_id)
    {
        $product = Product::find($product_id);
        $this->authorize('update', $product);

        $data = $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'quantity' => 'required',
            'price' => 'required',
        ]);

        $product->update($data);

        if ($request->hasFile('images')) {
            $product->images()->delete();
            $images = $this->uploadImages($request->file('images'));
            $product->images()->createMany($images);
        }
        $updatedProduct = Product::with('images')->find($product_id);

        return redirect()->route('products.show', $product_id)
                        ->with('updatedProduct', $updatedProduct);
    }


    public function buy(Request $request, $product_id)
    {
        $product = Product::find($product_id);
        $this->authorize('buy', $product);

        $store = Store::find($product->store_id);
        $walletOwner = User::find($store->user_id)->wallet;
        $user = Auth()->user();
        $wallet = $user->wallet;
        $productPrice = $request->qty * $product->price;

        if ($this->isQuantityNotEnough($request->qty, $product->quantity)) {
            return redirect()->back()->with('message', 'Quantity Not Enough');
        }

        if ($this->isBalanceNotEnough($productPrice, $wallet->balance)) {
            return redirect()->back()->with('message', 'Balance Not Enough');
        }

        if ($request->code) {
            return $this->processPurchaseWithDiscount($request, $product, $wallet, $walletOwner);
        }

        return $this->processRegularPurchase($request, $product, $wallet, $walletOwner);
    }

    protected function isQuantityNotEnough($requestedQty, $availableQty)
    {
        return $requestedQty > $availableQty;
    }

    protected function isBalanceNotEnough($productPrice, $walletBalance)
    {
        return $productPrice > $walletBalance;
    }

    protected function processPurchaseWithDiscount($request, $product, $wallet, $walletOwner)
    {
        $voucher = Voucher::where('code', $request->code)->first();

        if ($this->isValidVoucher($voucher)) {
            $discount = $this->calculateDiscount($productPrice, $voucher->percentage);
            $finalPrice = $productPrice - $discount;

            $this->deductBalance($wallet, $finalPrice);
            $this->updateProductQuantity($product, $request->qty);
            $this->updateVoucherTimes($voucher);
            $this->addBalance($walletOwner, $finalPrice);

            $this->notifyProductSold($product, $request->qty);

            return to_route('products.index', $product->store_id)->with('message', 'Purchased with discount');
        } else {
            return redirect()->back()->with('message', 'Your code is not correct');
        }
    }

    protected function isValidVoucher($voucher)
    {
        return $voucher && $voucher->times >= 1;
    }

    protected function calculateDiscount($productPrice, $percentage)
    {
        return $productPrice * $percentage / 100;
    }

    protected function deductBalance($wallet, $amount)
    {
        $wallet->balance -= $amount;
        $wallet->save();
    }

    protected function updateProductQuantity($product, $qty)
    {
        $product->quantity -= $qty;
        $product->save();
    }

    protected function updateVoucherTimes($voucher)
    {
        $voucher->times -= 1;
        $voucher->save();
    }

    protected function addBalance($walletOwner, $amount)
    {
        $walletOwner->balance += $amount;
        $walletOwner->save();
    }

    protected function processRegularPurchase($request, $product, $wallet, $walletOwner)
    {
        $productPrice = $request->qty * $product->price;

        $this->deductBalance($wallet, $productPrice);
        $this->updateProductQuantity($product, $request->qty);
        $this->addBalance($walletOwner, $productPrice);

        $this->notifyProductSold($product, $request->qty);

        return to_route('products.index', $product->store_id)->with('message', 'Buying succeeded without discount');
    }

    protected function notifyProductSold($product, $qty)
    {
        $product->store->user->notify(new ProductSold($product, (int) $qty));
    }


    private function uploadImages(array $images): array
    {
        $data = [];
        foreach ($images as $image) {
            $temp = [];
            $temp['path'] = $image->store('public');
            $data[] = $temp;
        }
        return $data;
    }

}


