<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\VoucherController;
use App\Models\Product;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Lognin and Register
Route::group(['middleware' => 'guest'], function () {
    // Auth
    Route::get('login', [AuthController::class, 'login'])->name('login');
    Route::post('signin', [AuthController::class, 'signin'])->name('signin');
    Route::get('register', [AuthController::class, 'register'])->name('register');
    Route::post('signup', [AuthController::class, 'signup'])->name('signup');
});

Route::group(['middleware' => 'auth'], function () {
    // Auth
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    // Stores
    Route::get('/', [StoreController::class, 'index'])->name('stores.index');

    Route::group(['prefix' => 'stores'] , function () {
        Route::get('/create', [StoreController::class, 'create'])->name('stores.create');
        Route::post('/store', [StoreController::class, 'store'])->name('stores.store');
        Route::get('/{store_id}/edit', [StoreController::class, 'edit'])->name('stores.edit');
        Route::post('/{store_id}/update', [StoreController::class, 'update'])->name('stores.update');
        Route::get('/{store_id}/destroy', [StoreController::class, 'destroy'])->name('stores.destroy');
    });

    // Products
    Route::get('/stores/{store_id}/products', [ProductController::class, 'index'])->name('products.index');
    Route::get('/stores/{store_id}/products/create', [ProductController::class, 'create'])->name('products.create');
    Route::post('/stores/{store_id}/products/store', [ProductController::class, 'store'])->name('products.store');
    Route::get('/products/{product_id}/show', [ProductController::class, 'show'])->name('products.show');
    Route::get('/products/{product_id}/destroy', [ProductController::class, 'destroy'])->name('products.destroy');
    Route::get('/products/{product_id}/edit', [ProductController::class, 'edit'])->name('products.edit');
    Route::post('/products/{product_id}/update', [ProductController::class, 'update'])->name('products.update');


    Route::post('/products/{product_id}/buy', [ProductController::class, 'buy'])->name('products.buy');

    //Stores

    // Notifications
    Route::get('/notifications/{user_id}', [NotificationController::class, 'index'])->name('notifications.index');
    Route::get('/notifications/{notification_id}/read', [NotificationController::class, 'read'])->name('notifications.read');
    Route::get('/notifications/{notification_id}/destroy', [NotificationController::class, 'destroy'])->name('notifications.destroy');

    // Vouchers
    Route::get('/stores/{store_id}/vouchers', [VoucherController::class, 'index'])->name('vouchers.index');
    Route::get('/stores/{store_id}/vouchers/create', [VoucherController::class, 'create'])->name(('vouchers.create'));
    Route::post('/stores/{store_id}/vouchers/store', [VoucherController::class, 'store'])->name(('vouchers.store'));
    Route::get('/vouchers/{voucher_id}/edit', [VoucherController::class, 'edit'])->name(('vouchers.edit'));
    Route::post('/vouchers/{voucher_id}/update', [VoucherController::class, 'update'])->name(('vouchers.update'));
    Route::get('/vouchers/{voucher_id}/destroy', [VoucherController::class, 'destroy'])->name(('vouchers.destroy'));

});
