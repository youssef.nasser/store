<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.css'>

    <title>Show</title>
</head>
<body>
    @if(session('message'))
        <div class="alert alert-success" style="color: red">
            {{ session('message') }}
        </div>
    @endif
    <h1>{{$product->name}}</h1>
    @can('viewAny', [App\Models\Store::class])
    <a href="{{ route('stores.index') }}"> My Stores</a> |
    @endcan
    @can('viewAny', $product)
        <a href="{{ route('products.index' ,$product->store_id)}}">My Products</a> |
    @endcan
    @cannot('viewAny', $product)
        <a href="{{route('products.index', $product->store_id)}}">Products</a> |
    @endcannot
    <a href="{{ route('stores.index') }}">Stores</a> |
    <a href="{{ route('notifications.index',$user->id ) }}"> Notifiction <span style="color: rgb(254, 203, 0)">({{ $notify_count }}) </span> </a>

    | <a href="{{ route('logout') }}">Logout</a>

    <span align="right" style="color: rgb(1, 191, 1)">
        {{ $wallet->balance }} $
    </span>
    <hr>
    @foreach($images as $image)
        <img src="{{ Storage::url($image->path) }}" alt="Product image" width="200" style="margin: 3px;">
    @endforeach
    <br>

    <p> <span style="font-size: 21px">Description : </span>{{ $product->description }}</p>

    <p> <span style="font-size: 21px">Quantity : </span>{{ $product->quantity }} </p>

    <p> <span style="font-size: 21px">Price : </span>{{ $product->price }} </p>

    @can('buy', [App\Models\Product::class])

    <details>
        <summary style="color: rgb(32, 135, 125)"> Buy Now</summary>
            <form action="{{route('products.buy', $product->id) }}" method="POST">
                @method('POST')
                @csrf
                <label for="qty">Quantity</label>
                <input type="number" id="qty" name="qty" required>

                <label for="code">Voucher Code</label>
                <input type="text" id="code" name="code">

                <button type="submit">Buy</button>
            </form>
        </details>
        @endcan
    </body>
</html>
