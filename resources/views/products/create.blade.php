<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.css'>
    <title>Create</title>
</head>
<body>
    <h1>New Product</h1>
    @can('viewAny', [App\Models\Store::class])
        <a href="{{ route('stores.index') }}"> My Stores</a> |
    @endcan
    @can('viewAny', [App\Models\Product::class])
        <a href="{{ route('products.index', $store_id) }}">My Products</a> |
    @endcan
    <a href="{{ route('notifications.index',$user_id ) }}"> Notifiction <span style="color: rgb(254, 203, 0)">({{ $notify_count }}) </span> </a>
 |
    <a href="{{route('logout')}}">Logout</a>
    <hr>
    <form  action="{{ route('products.store', $store_id) }}" method="POST" enctype="multipart/form-data">
        @csrf

        <label for="naem">Name</label>
        <input type="text" name="name" id="name" placeholder="Name" required>

        <label for="description">Description</label>
        <textarea name="description" id="description" cols="30" rows="10" placeholder="Description" required ></textarea>

        <label for="quantity">Quantity</label>
        <input type="number" id="quantity" name="quantity" placeholder="Quantity" required >

        <label for="price">Price</label>
        <input type="number" id="price" name="price" placeholder="Price" required >

        <label for="images">Images</label>
        <input type="file" id="images" name="images[]" multiple required>

        <button type="submit" name="create">Create</button>
    </form>

</body>
</html>
