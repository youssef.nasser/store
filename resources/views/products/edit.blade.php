<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.css'>
    <title>Edit</title>
</head>
<body>
    <h1>Edit {{$product->name}}</h1>
    @can('viewAny', [App\Models\Store::class])
        <a href="{{ route('stores.index') }}"> My Stores</a> |
    @endcan
    @can('viewAny', $product)
        <a href="{{ route('products.index' ,$product->store_id)}}">My Products</a> |
    @endcan
    <a href="{{ route('notifications.index',$user_id ) }}"> Notifiction <span style="color: rgb(254, 203, 0)">({{ $notify_count }}) </span> </a>

    | <a href="{{ route('logout') }}">Logout</a>
    <hr>
    <form  action="{{ route('products.update', $product->id) }}" method="POST" enctype="multipart/form-data">
        @csrf

        <label for="naem">Name</label>
        <input type="text" name="name" id="name" value="{{$product->name}}" >

        <label for="description">Description</label>
        <textarea name="description" id="description" cols="30" rows="10">{{ $product->description }}</textarea>

        <label for="quantity">Quantity</label>
        <input type="number" id="quantity" name="quantity" value="{{ $product->quantity }}">

        <label for="price">Price</label>
        <input type="number" id="price" name="price" value="{{ $product->price }}">

        @foreach($product->images as $image)
        <img src="{{ Storage::url($image->path) }}" alt="Product image" width="200" style="margin: 3px;">
        @endforeach

        <label for="images">Images</label>
        <input type="file" id="images" name="images[]" multiple>

        <button type="submit" name="update">Update</button>
    </form>

</body>
</html>
