<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.css'>
    <title>Products</title>
</head>
<body>
    @if(session('message'))
        <div class="alert alert-success" style="color: cornflowerblue">
            {{ session('message') }}
        </div>
    @endif

    <h1>products</h1>

    @can('create', [App\Moldes\Product::class])
        <a href="{{ route('products.create', $store_id ) }}">New Product</a> |
    @endcan
    @can('create', [App\Moldes\Product::class])
        <a href="{{ route('vouchers.index', $store_id ) }}">Vouchers</a> |
    @endcan
    <a href="{{ route('stores.index') }}">Stores</a> |
    <a href="{{ route('notifications.index', $user->id ) }}"> Notifiction <span style="color: rgb(254, 203, 0)">({{ $notify_count }}) </span> </a>

    | <a href="{{route('logout')}}">Logout</a>
    | Balance :
    <span align="right" style="color: rgb(1, 191, 1)">
        {{ $wallet->balance }} $
    </span>
    <hr>
    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Description</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $key => $product)
                <tr>
                    <td>{{ $key + 1  }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ \Illuminate\Support\Str::limit($product->description, 15) }}</td>
                    <td>{{ $product->quantity }}</td>
                    <td>{{ $product->price }}</td>
                    <td>
                        @if ($user->is_owner)
                        <a href="{{ route('products.show', $product->id) }}">View</a>
                        @else
                        <a href="{{ route('products.show', $product->id) }}">Open</a>
                        @endif
                        @can('update', $product)
                        | <a href="{{ route('products.edit', $product->id) }}">Edit</a>
                        @endcan
                        @can('delete',$product )
                            | <a href="{{ route('products.destroy', $product->id) }}" onclick="sure()">delete</a>
                        @endcan
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>

<script>
    function sure() {
        alert("You have deleted product");
    }
</script>
