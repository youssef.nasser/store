<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.css'>
    <title>Create</title>
</head>
<body>
    <h1>New store </h1>
    @can('viewAny', [App\Models\Store::class ])
        <a href="{{ route('stores.index') }}">My stores </a> |
    @endcan
    <a href="#">Notifiction <span style="color: rgb(254, 203, 0)">({{ $notify_count }}) </span> </a>
 |
    <a href="{{ route('logout') }}">Logout</a>
    <hr>
    <form action="{{ route('stores.store') }}"   method="POST" enctype="multipart/form-data">
    @csrf

        <label for="name">Name</label>
        <input type="text" id="name" name="name" placeholder="Name">

        <label for="about">About</label>
        <input type="text" id="about" name="about" placeholder="About">

        <label for="image">Image</label>
        <input type="file" name="image" id="image">

        <label for="phone">Phone</label>
        <input type="text" id="phone" name="phone" placeholder="Phone">
        <br>
        <button type="submit" name="submit"onclick="sure()">Create</button>
    </form>
</body>
</html>

<script>
    function sure() {
        alert("We will discount 10 pounds from your balance");
    }
</script>
