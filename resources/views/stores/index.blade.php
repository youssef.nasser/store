<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.css'>
    <title>Document</title>
</head>
<body>
    @if(session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    <h1>Store</h1>

    <a href="{{route('logout')}}">Logout</a>

    @can('create', [App\Models\Store::class])
        | <a href="{{ route('stores.create') }}"> New Store </a>
    @endcan
    |<a href="{{ route('notifications.index',$user_id ) }}"> Notifiction <span style="color: rgb(254, 203, 0)">({{ $notify_count }}) </span> </a>

    | Balance :
    <span align="right" style="color: rgb(1, 191, 1)">
        {{$wallet->balance }} $
    </span>

    <hr>
    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>About</th>
                <th>Phone</th>
                <th>Image</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($stores as $key => $store)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $store->name }}</td>
                    <td>{{ $store->about }}</td>
                    <td>{{ $store->phone }}</td>
                    @if ($store->image)
                    <td><img src="/uploads/stores/{{ $store->image }} " height="60px" width="60px"/></td>
                    @else
                    <td><img src="/uploads/stores/default.png " height="60px" width="60px"/></td>
                    @endif
                    <td>
                        <a href="{{ route('products.index', $store->id ) }}">view</a>

                        @can('update', [App\Models\Store::class, $store])
                            | <a href="{{ route('stores.edit', $store->id )}}">
                                Edit
                            </a>
                        @endcan
                        @can('delete', [App\Models\Store::class, $store] )
                            | <a href="{{ route('stores.destroy', $store->id )}}" onclick="sure()" >Delete</a>
                        @endcan
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
<script>
    function sure() {
        alert("You have deleted store");
    }
</script>
