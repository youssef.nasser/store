<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.css'>
    <title>Edit</title>
</head>
<body>
    <h1>{{ $store->name}} </h1>
    @can('viewAny', [App\Models\Store::class])
        <a href="{{ route('stores.index') }}"> My Stores</a> |
    @endcan
    <a href="{{ route('notifications.index',$store->user_id ) }}"> Notifiction <span style="color: rgb(254, 203, 0)">({{ $notify_count }}) </span> </a>

    | <a href="{{ route('logout') }}">Logout</a>
    <hr>
    <form action="{{ route('stores.update', $store->id ) }}"   method="POST" enctype="multipart/form-data">
    @csrf

        <label for="name">Name</label>
        <input type="text" id="name" name="name" value="{{ $store->name }}">

        <label for="about">About</label>
        <input type="text" id="about" name="about" value="{{ $store->about }}">
        <br>

        @if ($store->image)
            <img src="/uploads/stores/{{ $store->image }} " height="60px" width="60px"/>
        @else
            <img src="/uploads/stores/default.png " height="60px" width="60px"/>
        @endif
        <br>

        <label for="image">Image</label>
        <input type="file" name="image" id="image" >


        <label for="phone">Phone</label>
        <input type="text" id="phone" name="phone" value="{{ $store->phone }}">
        <br>
        <button type="submit" name="submit">Update</button>

    </form>
</body>
</html>
