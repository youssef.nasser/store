<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.css'>
    <title>Notification</title>
</head>
<body>
    <h1>Notifications {{ $user->name . " ( " . $notifications->whereNull('read_at')->count()  ." )"}} </h1>
    @can('viewAny', [App\Models\Store::class])
        <a href="{{ route('stores.index') }}"> My Stores</a> |
    @endcan
    <a href="{{ route('logout') }}">Logout</a>
    <hr>

    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>Type</th>
                <th>Title</th>
                <th>Message</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($notifications as $key => $notification)
            <tr>
                <td>{{ $key + 1 }}</td>
                @if ($notification->type == "App\Notifications\UserRegistered")
                    <td> User Registered </td>
                @else
                    <td> Product Sold</td>
                @endif
                <td>{{ $notification->data['title'] }}</td>
                <td>{{ $notification->data['message'] }}</td>
                @if (  !$notification->read_at )
                    <td>
                        <a href="{{ route('notifications.read', [
                            'notification_id' => $notification->id ])}}">
                            Read
                        </a>
                    </td>
                @else
                    <td>
                        <a href="{{ route('notifications.destroy', [
                            'notification_id' => $notification->id ])}}" onclick="sure()">
                            Delete
                        </a>
                    </td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
    <script>
        function sure() {
            alert("You have deleted notification");
        }
    </script>
</body>
</html>
