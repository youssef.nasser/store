<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.css'>
    <title>Create</title>
</head>
<body>
    <h1>Edit Vouchers</h1>
    @can('viewAny', [App\Models\Store::class])
        <a href="{{ route('stores.index') }}"> My Stores</a> |
    @endcan
    <a href="{{ route('vouchers.index', $store->id) }}">Vouchers</a>
    | <a href="{{ route('notifications.index',$user_id ) }}"> Notifiction <span style="color: rgb(254, 203, 0)">({{ $notify_count }}) </span> </a>
    | <a href="{{ route('logout') }}">Logout</a>

    <hr>

    <form action="{{ route('vouchers.update', $voucher->id) }}" method="POST">
        @csrf

        <label for="percentage">Percentage</label>
        <input type="number" id="percentage" name="percentage" value="{{ $voucher->percentage}}" required>

        <label for="times">Times</label>
        <input type="number" id="times" name="times" value="{{ $voucher->times }}" required>

        <label for="code">Code</label>
        <input type="code" id="code" name="code" value="{{ $voucher->code }}" required>

        <button type="submit">Update</button>
    </form>
</body>
</html>
