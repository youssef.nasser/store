<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.css'>
    <title>Vouchers</title>
</head>
<body>
    <h1>Vouchers {{ $store->name }}</h1>
    <a href="{{ route('vouchers.create', $store->id) }}">New Voucher</a> |
    <a href="{{ route('products.index', $store->id) }}">My Products</a> |
    @can('viewAny', [App\Models\Store::class])
        <a href="{{ route('stores.index') }}"> My Stores</a> |
    @endcan
    <a href="{{ route('notifications.index',$store->user_id ) }}"> Notifiction <span style="color: rgb(254, 203, 0)">({{ $notify_count }}) </span> </a>
    | <a href="{{ route('logout') }}">Logout</a>

    <hr>
    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>Store</th>
                <th>Percentage</th>
                <th>Times</th>
                <th>Code</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($vouchers as $key => $voucher)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $store->name }}</td>
                    <td>{{ $voucher->percentage }}</td>
                    <td>{{ $voucher->times }}</td>
                    <td>{{ $voucher->code }}</td>
                    <td>
                        <a href="{{ route('vouchers.edit', $voucher->id) }}">Edit</a>
                        | <a href="{{ route('vouchers.destroy', $voucher->id)}}">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
