<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.css'>
    <title>Create</title>
</head>
<body>
    <h1>Create Vouchers  {{ $store->name}} </h1>
    <a href="{{ route('products.index', $store->id) }}">My Products</a> |
    @can('viewAny', [App\Models\Store::class])
        <a href="{{ route('stores.index') }}"> My Stores</a> |
    @endcan
    <a href="{{ route('notifications.index',$store->user_id ) }}"> Notifiction <span style="color: rgb(254, 203, 0)">({{ $notify_count }}) </span> </a>

    | <a href="{{route('logout')}}">Logout</a>
    <hr>

    <form action="{{ route('vouchers.store', $store->id) }}" method="POST">
        @csrf

        <label for="percentage">Percentage</label>
        <input type="number" id="percentage" name="percentage" placeholder="percentage" required>

        <label for="times">Times</label>
        <input type="number" id="times" name="times" placeholder="times" required>

        <label for="code">Code</label>
        <input type="code" id="code" name="code" placeholder="code" required>

        <button type="submit">Create</button>
    </form>
</body>
</html>
