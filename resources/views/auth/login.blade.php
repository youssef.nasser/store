<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.css'>
    <title>Login</title>
</head>
<body>
    <h1>Login</h1>
    <a href="{{ route('register') }}">Register</a>
    <hr>
    <form action="{{ route('signin') }}" method="POST">
        @csrf

        <label for="email">Email</label>
        <input type="email" id="email" name="email" placeholder="email">

        <label for="password">Password</label>
        <input type="password" id="password" name="password" placeholder="password">

        <button type="submit" name="submit">Login</button>
    </form>
</body>
</html>
