<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.css'>
    <title>Signup</title>
</head>
<body>
    <h1>Register</h1>
    <a href="{{ route('login') }}">Login</a>
    <hr>
    <form action="{{ route('signup') }}" method="POST">
        @csrf
        <label for="name">Name</label>
        <input type="text" id="name" name="name" placeholder="Name" required>

        <label for="email">Email</label>
        <input type="email" id="email" name="email" placeholder="Email" required>

        <label for="password">Password</label>
        <input type="password" id="password" name="password" placeholder="Password" required>

        <label for="password_confirmed">Confirm Password</label>
        <input type="password" id="password_confirmed" name="password_confirmation" placeholder="Password confirm" required>


        <label for="balance">Balance</label>
        <input type="number" id="balance" name="balance" placeholder="Balance">


        <label for="is_owner">Is Owner</label>
        <input type="checkbox" id="is_owner" name="is_owner" >
        <br>

        <button type="submit" name="signup">Signup</button>
    </form>
</body>
</html>
